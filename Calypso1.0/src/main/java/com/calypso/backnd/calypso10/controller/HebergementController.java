package com.calypso.backnd.calypso10.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.calypso.backnd.calypso10.beans.Hebergement;
import com.calypso.backnd.calypso10.hebergementservice.IHebergementService;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;


@RestController
@CrossOrigin("http://localhost:4200")
public class HebergementController {
	
	/* injection de notre couche service */
	@Autowired 
	IHebergementService iHebergementService;
	
	/**
	 * Recuperation de tous les utilisateurs
	 * @return Une liste qui contient tous les hebergement
	 */
	@GetMapping("/Calypso/RechercheHebergement")
	public MappingJacksonValue rechercheHebergement()
	{
		List <Hebergement> listHebergement = iHebergementService.recherche();
		SimpleBeanPropertyFilter hebergementFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","voyage");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("hebergementFiltre", hebergementFiltre);
		MappingJacksonValue hebergementJacksonValue = new MappingJacksonValue(listHebergement);
		hebergementJacksonValue.setFilters(listeFilterProvider);
		return  hebergementJacksonValue;
	}
	
	/**
	 * Recuperation des hebergements par leur ID
	 * @param id represente l'id de l'hebergement
	 * @return Un hebergement par son id
	 */
	@GetMapping("/Calypso/RechercheHebergement/{id}")
	public MappingJacksonValue hebergementId(@PathVariable Long id)
	{
		Hebergement hebergementVerification = iHebergementService.rechercheId(id);
		if(hebergementVerification.getNom().equals("emptycase"))
		{
			SimpleBeanPropertyFilter hebergementFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","voyage","pays","region","adresse","nom","type");
			FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("hebergementFiltre", hebergementFiltre);
			MappingJacksonValue hebergementJacksonValue = new MappingJacksonValue(hebergementVerification);
			hebergementJacksonValue.setFilters(listeFilterProvider);
			return  hebergementJacksonValue;
		}
		SimpleBeanPropertyFilter hebergementFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","voyage");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("hebergementFiltre", hebergementFiltre);
		MappingJacksonValue hebergementJacksonValue = new MappingJacksonValue(hebergementVerification);
		hebergementJacksonValue.setFilters(listeFilterProvider);
		return  hebergementJacksonValue;
			
	}

	
	/**
	 * Recuperation des hebergements par pays
	 * @param pays represente le pays ou l'hebergement se situe
	 * @return Une liste d'hebergement dans le pays
	 */
	@GetMapping("/Calypso/RechercheHebergementPays/{pays}")
	public MappingJacksonValue hebergementPays(@PathVariable String pays)
	{	
		List <Hebergement> listHebergement = iHebergementService.recherchePays(pays);
		SimpleBeanPropertyFilter hebergementFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","voyage", "theme", "utilisateur");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("hebergementFiltre", hebergementFiltre);
		MappingJacksonValue hebergementJacksonValue = new MappingJacksonValue(listHebergement);
		hebergementJacksonValue.setFilters(listeFilterProvider);
		return  hebergementJacksonValue;
	}
	
	/**
	 * Recuperation des hebergements par region
	 * @param region represente la region ou l'hebergement se situe 
	 * @return Une liste d'hebergement dans la region choisie
	 */
	@GetMapping("/Calypso/RechercheHebergementRegion/{region}")
	public MappingJacksonValue hebergementRegion(@PathVariable String region)
	{	
		List <Hebergement> listHebergement = iHebergementService.rechercheRegion(region);
		SimpleBeanPropertyFilter hebergementFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","voyage", "theme", "utilisateur");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("hebergementFiltre", hebergementFiltre);
		MappingJacksonValue hebergementJacksonValue = new MappingJacksonValue(listHebergement);
		hebergementJacksonValue.setFilters(listeFilterProvider);
		return  hebergementJacksonValue;
	}
	
	/**
	 * Recuperation d'un hebergement par son adresse
	 * @param adresse represente l'adresse de l'hebergement
	 * @return Un hebergement selon son adresse
	 */
	@GetMapping("/Calypso/RechercheHebergementAdresse/{adresse}")
	public MappingJacksonValue hebergementAdresse(@PathVariable String adresse)
	{	
		Hebergement listHebergement = iHebergementService.rechercheAdresse(adresse);
		SimpleBeanPropertyFilter hebergementFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","voyage", "theme", "utilisateur");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("hebergementFiltre", hebergementFiltre);
		MappingJacksonValue hebergementJacksonValue = new MappingJacksonValue(listHebergement);
		hebergementJacksonValue.setFilters(listeFilterProvider);
		return  hebergementJacksonValue;
	}
	
	/**
	 * Recuperation des hebergements par leur nom
	 * @param nom represente le nom de l'hebergement
	 * @return Une liste d'hebergement ayant potentiellement le meme nom
	 */
	@GetMapping("/Calypso/RechercheHebergementNom/{nom}")
	public List <Hebergement> hebergementNom(@PathVariable String nom)
	{	
		return  iHebergementService.rechercheNom(nom);
	}
	
	/**
	 * Recuperation des hebergements par leur type
	 * @param type represente la categorie de l'hebergement par ses etoiles
	 * @return Une liste d'hebergement selon la categorie
	 */
	@GetMapping("/Calypso/RechercheHebergementType/{type}")
	public MappingJacksonValue hebergementType(@PathVariable String type)
	{	
		List <Hebergement> listHebergement = iHebergementService.rechercheType(type);
		SimpleBeanPropertyFilter hebergementFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","voyage", "theme", "utilisateur");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("hebergementFiltre", hebergementFiltre);
		MappingJacksonValue hebergementJacksonValue = new MappingJacksonValue(listHebergement);
		hebergementJacksonValue.setFilters(listeFilterProvider);
		return  hebergementJacksonValue;
	}
	
	/**
	 * Permet la sauvegarde d'un nouveau hebergement
	 * @param hebergement Represente l'objet à sauvegarder
	 */
	@PostMapping("/Calypso/NouveauHebergement")
    public ResponseEntity<Void> sauvegardeHebergement(@RequestBody Hebergement hebergement )
    {
         boolean verificationSauvegarde = iHebergementService.sauvegardeHebergement(hebergement);
         if(verificationSauvegarde!=false)
         {
        	 URI hebergementSauvegardeUri = ServletUriComponentsBuilder
        			 .fromCurrentRequest()
        			 .path("{/id}")
        			 .buildAndExpand(hebergement.getId())
        			 .toUri();
        	return  ResponseEntity.created(hebergementSauvegardeUri).build();
         }
         return ResponseEntity.noContent().build();
    }
	
	/**
	 * Permet la supression d'un nouveau hebergement
	 * @param transporteurID Represente l'id de l'objet a supprimer
	 */
	@PostMapping("/Calypso/SuppressionHebergement")
	public ResponseEntity<Void> supprimerHebergement(@RequestBody TransporteurID transporteurID)
	{
		boolean confirmationSuppression = iHebergementService.suppressionHebergement(transporteurID);	
		if(confirmationSuppression!=false)
		{
			URI hebergementSuppression = ServletUriComponentsBuilder
					.fromCurrentRequest()
					.path("/{id}")
					.buildAndExpand(transporteurID.getId())
					.toUri();
			return ResponseEntity.created(hebergementSuppression).build();
		}
		return ResponseEntity.noContent().build();
	}
	

}
