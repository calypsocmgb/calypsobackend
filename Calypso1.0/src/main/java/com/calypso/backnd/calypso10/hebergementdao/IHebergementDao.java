package com.calypso.backnd.calypso10.hebergementdao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.calypso.backnd.calypso10.beans.Hebergement;

/**
 * L'interface IHebergementDao represente notre couche interrogeant la BDD afin
 * de recuperer les informations necessaire a l'application
 * @author Groupe Calypso
 *
 */


@Repository
public interface IHebergementDao extends JpaRepository<Hebergement, Long> {
	
	/**
	 * Requete qui permet de trouver des etablissements par pays
	 * @param pays Represente le pays recherche
	 * @return Une liste d'hebergement selon le pays 
	 */
	@Query("select h from Hebergement h where h.pays = :pays")
	List <Hebergement> recherchePays(@Param ("pays") String pays) ;
	
	/**
	 * Requete qui permet de trouver des etablissements par region
	 * @param region Represente la region recherchee
	 * @return Une liste d'hebergement selon la region
	 */
	@Query("select h from Hebergement h where h.region = :region")
	List <Hebergement> rechercheRegion(@Param ("region") String region) ;
	
	/**
	 * Requete qui permet de trouver un etablissement par son adresse
	 * @param adresse Represente l'adresse recherchee
	 * @return Un hebergement par son adresse
	 */
	@Query("select h from Hebergement h where h.adresse = :adresse")
	Hebergement rechercheAdresse(@Param ("adresse") String adresse) ;
	
	/**
	 * Requete qui permet de trouver des etablissements par leur nom
	 * @param nom Represente le nom recherche
	 * @return Une liste d'hebergement selon le nom en prevoyant des homonymes
	 */
	@Query("select h from Hebergement h where h.nom = :nom")
	List <Hebergement> rechercheNom(@Param ("nom") String nom) ;
	
	/**
	 * Requete qui permet de trouver des etablissements par leur type
	 * @param type Represente le type recherche
	 * @return Une liste d'hebergement selon leur type
	 */
	@Query("select h from Hebergement h where h.type = :type")
	List <Hebergement> rechercheType(@Param ("type") String type) ;

}
