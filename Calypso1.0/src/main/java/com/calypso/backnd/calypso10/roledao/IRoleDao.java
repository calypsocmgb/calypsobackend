package com.calypso.backnd.calypso10.roledao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.calypso.backnd.calypso10.beans.Role;

	/**
	 * L'interface RoleDao represente notre couche DAO permettant le dialogue avec la base de donnee
	 * @author Groupe Calypso
	 *
	 */

@Repository
public interface IRoleDao extends JpaRepository<Role, Long>{

	
	/**
	 * La requete nous permet de retrouver un objet present en base de donnee avec un parametre
	 * @param nom represente le parametre associe a la requete
	 * @return une liste de nos objets correspondant au parametre precise dans la requete
	 */
	@Query(value = "SELECT id,role from role where role.role =:nom",nativeQuery = true)
	public List<Role> rechercheRoleName(@Param("nom")String nom);
}
