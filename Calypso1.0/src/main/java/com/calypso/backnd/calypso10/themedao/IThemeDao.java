package com.calypso.backnd.calypso10.themedao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.calypso.backnd.calypso10.beans.Theme;

/**
 * la classe themedao va nous permettre de gerer les interactions avec la base de donee 
 * @author Groupe Calypso
 *
 */

@Repository
public interface IThemeDao extends JpaRepository<Theme, Long>
{
	
	
	@Query(value = "SELECT id,theme from public.theme where theme.theme=:nom", nativeQuery = true)
	public List<Theme> rechercheListeTheme(@Param("nom")String nom);

}
