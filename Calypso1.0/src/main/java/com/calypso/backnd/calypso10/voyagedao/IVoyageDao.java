package com.calypso.backnd.calypso10.voyagedao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.calypso.backnd.calypso10.beans.Voyage;

/**
 * L'interface VoyageDao nous permet d'attaquer 
 * la BDD afin de recuperer les informations 
 * dont nous avons besoin
 * @author Groupe Calypso
 *
 */

@Repository
public interface IVoyageDao extends JpaRepository<Voyage, Long>{

	/**
	 * La requete nous permet de selectionner les voyages en fonction de leur nom
	 * @param voyageNom represente le nom des voyages que l'on veut retrouver
	 * @return une liste de voyage
	 */
	@Query(value = "SELECT voyage_id, date_debut, date_fin, itinerance, nom, nombre_voyageur, prix, theme_id, pension_id, utilisateur_id, pays, detail FROM voyage where voyage.nom = :nomVoyage", nativeQuery = true)
	public List<Voyage> rechercheVoyageNom(@Param ("nomVoyage") String voyageNom);
	
	
	/**
	 * La requete nous permet de selectionner les voyages en fonction de leur pays
	 * @param pays represente le pays des voyages que l'on veut retrouver
	 * @return une liste de voyage
	 */
	@Query(value = "SELECT voyage_id, date_debut, date_fin, itinerance, nom, nombre_voyageur, prix, theme_id, pension_id, utilisateur_id, pays, detail FROM voyage where voyage.pays = :pays", nativeQuery = true)
	public List<Voyage> rechercheVoyagePays(@Param ("pays") String pays);
	
	
	
	
}
