package com.calypso.backnd.calypso10.controller;

import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.calypso.backnd.calypso10.beans.Role;
import com.calypso.backnd.calypso10.roleservice.IRoleService;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;


/**
 * La RoleController va nous permettre de gerer les requetes arrivant
 * de notre front end et de traiter celles ayant des interactions 
 * avec les objets de types roles. 
 * @author Groupe Calypso
 *
 */

@RestController
@CrossOrigin("*")
public class RoleController {

	
	/*injection de notre couche service*/
	@Autowired
	IRoleService roleService;
	
	
	/**
	 * La methode va nous permettre de retourner une liste des objets present en base de donnee
	 * @param tHeaders represente les headers de la requetes
	 * @return une liste de nos objets filtre par json
	 */
	@GetMapping("/Calypso/RoleListe")
	public MappingJacksonValue rechercheRoleListe(@RequestHeader HttpHeaders httpHeaders)
	{
		 
		/*reponse*/
		List<Role> listeRoleRecuperation = roleService.rechercheRoleListe();
		SimpleBeanPropertyFilter roleListe = SimpleBeanPropertyFilter.serializeAllExcept("id","utilisateur");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("roleFiltre", roleListe);
		MappingJacksonValue roleJacksonValue = new MappingJacksonValue(listeRoleRecuperation);
		roleJacksonValue.setFilters(listeFilterProvider);
		return roleJacksonValue;
		
	}
	
	
	/**
	 * La methode va nous permettre de recherche un objet en BDD grace a un parametre
	 * @param httpHeaders represente les headers de la requete attaquante
	 * @param id represente le parametre associe a la recherche de notre objet
	 * @return l'objet si le parametre est correct
	 */
	@GetMapping("/Calypso/Role/{id}")
	public MappingJacksonValue rechercheRoleID(@RequestHeader HttpHeaders httpHeaders,@PathVariable Long id)
	{
		Role role = roleService.rechercheRoleID(id);
		if (role.getRole().equals("emptycase")) {
			SimpleBeanPropertyFilter roleRecherche = SimpleBeanPropertyFilter.serializeAllExcept("id","utilisateur","role");
			FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("roleFiltre", roleRecherche);
			MappingJacksonValue roleJacksonValue = new MappingJacksonValue(role);
			roleJacksonValue.setFilters(listeFilterProvider);
			return roleJacksonValue;
		}

		SimpleBeanPropertyFilter roleRecherche = SimpleBeanPropertyFilter.serializeAllExcept("id","utilisateur");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("roleFiltre", roleRecherche);
		MappingJacksonValue roleJacksonValue = new MappingJacksonValue(role);
		roleJacksonValue.setFilters(listeFilterProvider);
		return roleJacksonValue;
	}
	
	/**
	 * La methode nous permet de retrouver une liste de nos objets avec un parametre associe
	 * @param httpHeaders represente les headers de la requete attaquante 
	 * @param nom represente le parametre associe avec la requete en bdd 
	 * @return une liste de nos objets si le parametre est correct
	 */
	@GetMapping("/Calypso/RoleName/{nom}")
	public MappingJacksonValue rechercheRoleNom(@RequestHeader HttpHeaders httpHeaders, @PathVariable String nom)
	{
		List<Role> rechercheRoleNameList = roleService.rechercheRoleName(nom);
		SimpleBeanPropertyFilter roleListe = SimpleBeanPropertyFilter.serializeAllExcept("id","utilisateur");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("roleFiltre", roleListe);
		MappingJacksonValue roleJacksonValue = new MappingJacksonValue(rechercheRoleNameList);
		roleJacksonValue.setFilters(listeFilterProvider);
		return roleJacksonValue;
	}
	
	/**
	 * La methode nous permet de persister un objet en base de donnee 
	 * @param httpHeaders represente les headers de la requete attaquante
	 * @param role represente le type de l'objet que l'on veut persister en base de donnee
	 * @return un URI construit afin d'obtenir des informations sur l'operation
	 */
	@PostMapping("/Calypso/RoleSave/Sauvegarde")
	public ResponseEntity<Void> sauvegardeRolEntity(@RequestHeader HttpHeaders httpHeaders, @RequestBody Role role)
	{
		if(role.getId()==null) {
		boolean roleSauvegardeVerification = roleService.roleSauvegarde(role);
		if(roleSauvegardeVerification!=false)
		{
			URI roleConstruction = ServletUriComponentsBuilder
					.fromCurrentRequest()
					.path("/{id}")
					.buildAndExpand(role.getId())
					.toUri();
			return ResponseEntity.created(roleConstruction).build();
		}}
		return ResponseEntity.status(403) .build();
	}
	
	/**
	 * La methode permet la suppression d'un objet en base de donnee
	 * @param httpHeaders represente les headers de notre requete attaquante 
	 * @param transporteurID represente le parametre associe a notre requete
	 * @return une URI construite sur les informations de requete retour
	 */
	@PostMapping("/Calypso/Role/SuppressionRole")
	public ResponseEntity<Void> suppressionRole(@RequestHeader HttpHeaders httpHeaders,@RequestBody TransporteurID transporteurID)
	{
		boolean roleSuppressionConfirmation = roleService.suppressionRole(transporteurID);
		if(roleSuppressionConfirmation!=false)
		{
			URI roleSuppression = ServletUriComponentsBuilder
					.fromCurrentRequest()
					.path("/{id}")
					.buildAndExpand(transporteurID.getId())
					.toUri();
			return ResponseEntity.created(roleSuppression).build();
		}
		return ResponseEntity.status(403).build();
	}
}
