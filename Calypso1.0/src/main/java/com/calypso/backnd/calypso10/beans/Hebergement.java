package com.calypso.backnd.calypso10.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFilter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="hebergement")
@AllArgsConstructor
@Getter
@Setter
@SequenceGenerator(name = "id_hebergement_seq",allocationSize = 1)
@JsonFilter("hebergementFiltre")
public class Hebergement {

	@Id	
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "id_hebergement_seq")
	private int id;
	@Column
	private String pays;
	@Column
	private String region;
	@Column
	private String adresse;
	@Column
	private String nom;
	@Column
	private String type;
	
	@ManyToOne
	private Voyage voyage;
	
	
	public Hebergement()
	{
		this.id=0;
		this.pays="";
		this.region="";
		this.adresse="";
		this.nom="";
		this.type="";
		
	}
	
	
}
