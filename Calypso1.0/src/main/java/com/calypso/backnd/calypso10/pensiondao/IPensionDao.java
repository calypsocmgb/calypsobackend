package com.calypso.backnd.calypso10.pensiondao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.calypso.backnd.calypso10.beans.Pension;


/**
 * L'interface Pension Dao va nous permettre de gerer les requetes avec la BDD
 * @author Groupe Calypso
 *
 */
@Repository
public interface IPensionDao extends JpaRepository<Pension,Long>
{

	@Query(value = "SELECT id, 'type' FROM public.pension where pension.type=:nom", nativeQuery = true)	
	public List<Pension> rechercheName(@Param("nom") String nom);
	
}
