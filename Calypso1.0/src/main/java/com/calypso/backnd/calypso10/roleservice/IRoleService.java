package com.calypso.backnd.calypso10.roleservice;

import java.util.List;

import com.calypso.backnd.calypso10.beans.Role;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;

/**
 * L'interface IRoleService represente
 * @author user_onyx
 *
 */


public interface IRoleService {

	/**
	 * La methode nous permet de recuperer tout nos objet present en BDD
	 * @return une liste de nos objet present en base de donnee
	 */
	public List<Role> rechercheRoleListe();
	
	
	/**
	 * La methode nous permet de rechercher un objet en base de donnee grace a un parametre associes
	 * a celui-ci
	 * @param id represente le parametre associe a la recherche de notre id
	 * @return notre objet si le parametre rentre est correct
	 */
	public Role rechercheRoleID(Long id);
	
	
	/**
	 * La methode nous permet de retrouver nos objets en base de donnee avec un parametre preciser
	 * @param nom represente le parametre associe a la requete 
	 * @return une liste de nos objets correspondant a la requete associe au parametre 
	 */
	public List<Role> rechercheRoleName(String nom);
	
	
	/**
	 * La methode nous permet de persister nos objet en base de donnee
	 * @param role represente l'objet que l'on veut persister en bdd
	 * @return un booleen afin de verifier l'operation
	 */
	public Boolean roleSauvegarde(Role role);

	/**
	 * La methode nous permet de supprimer un objet persister en BDD
	 * @param transporteurID represente notre parametre associé a la requete
	 * @return un booleen afin d'obtenir des information sur l'operation
	 */
	public Boolean suppressionRole(TransporteurID transporteurID);

}
