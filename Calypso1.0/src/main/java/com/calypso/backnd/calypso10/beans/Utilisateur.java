package com.calypso.backnd.calypso10.beans;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFilter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "utilisateur")
@SequenceGenerator(name = "id_utilisateur_seq",allocationSize = 1)
@JsonFilter("filtreUtilisateur")
public class Utilisateur {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "id_utilisateur_seq")
	private Long id;
	@Column
	private String nom; 
	@Column
	private String prenom; 
	@Column
	private String adresse;
	@Column
	private String age;
	@Column
	private String mail;
	@Column
	private String telephone; 
	
	@OneToOne(cascade = CascadeType.ALL)
	Credential credential;
	
	@OneToMany(mappedBy = "utilisateur",cascade = CascadeType.ALL)
	@JsonBackReference(value = "utilisateur_back")
	private List<Voyage> voyage = new ArrayList<Voyage>();
	
	@ManyToOne
	Role role;
	
}
