package com.calypso.backnd.calypso10.activiteService;

import java.util.List;

import com.calypso.backnd.calypso10.beans.Activite;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;

/**
 * L'interface IActiviteService represente la couche métier
 * de notre modele mvc permettant la passerelle avec la couche
 * dao
 * @author Groupe Calypso
 *
 */

public interface IActiviteService {
	
	/**
	 * Permet de recuperer toutes les activites
	 * @return Une liste de toutes les activites
	 */
	public List<Activite> recherche();
	
	/**
	 * Permet de recuperer une activite par son id
	 * @param id Represente l'id d'une activite
	 * @return Une activite par son id
	 */
	public Activite rechercheId(Long id);
	
	/**
	 * Permet de recuperer des activites par leur nom
	 * @param nom Represente le nom d'une activite
	 * @return Une liste d'activites avec le nom desire
	 */
	public List<Activite> rechercheNom(String nom);
	
	/**
	 * Permet de recuperer une activite par son adresse
	 * @param adresse Represente l'adresse de l'activite
	 * @return Une activite selon son adresse
	 */
	public Activite rechercheAdresse(String adresse);
	
	/**
	 * Permet de recuperer des activites selon le pays
	 * @param pays Represente le pays de l'activite
	 * @return Une liste d'activite selon le pays desire
	 */
	public List<Activite> recherchePays(String pays);
	
	/**
	 * Permet de recuperer des activites selon la region
	 * @param region Represente la region de l'activite
	 * @return Une liste d'activite selon la region desiree
	 */
	public List<Activite> rechercheRegion(String region);
	
	/**
	 * Permet de sauvegarder une activite
	 * @param activite Represente l'objet a sauvegarder
	 * @return Confirmation ou non de la sauvagarde
	 */
	public boolean sauvegardeActivite(Activite activite);
	
	/**
	 * Permet de supprimer une activite
	 * @param transporteurID  Represente l'id de l'activite a supprimer
	 * @return Confirmation ou non de la suppression
	 */
	public boolean suppressionActivite (TransporteurID transporteurID);
	

	
}
