package com.calypso.backnd.calypso10.transporteur;

import lombok.Data;

/**
 * La classe Transporteur nous permet de manier les JSON
 * @author Groupe Calypso
 *
 */

@Data
public class TransporteurID {

	
	private Long id;
	
	
}
