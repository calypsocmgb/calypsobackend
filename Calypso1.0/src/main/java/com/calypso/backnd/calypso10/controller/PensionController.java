package com.calypso.backnd.calypso10.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.calypso.backnd.calypso10.beans.Pension;
import com.calypso.backnd.calypso10.pensionservice.IPensionService;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

/**
 * La classe PensionController nous permet de gerer les interactions avec
 * notre objet pension et sa persistance en BDD
 * @author Groupe Calypso 
 *
 */

@RestController
@CrossOrigin("http://localhost:4200")
public class PensionController {

	/*injection de notre interface*/
	@Autowired
	IPensionService pensionService;
	
	
	/**
	 * La methode nous permet d'interroger la base de donnee afin de recuperer une liste de nos objets
	 * @return une liste de nos objets pension persiste en bdd
	 */
	@GetMapping("/Calypso/PensionListe")
	public MappingJacksonValue recherchePensionListe()
	{
		List<Pension> pensionRecuperationList = pensionService.rechercheListePension();
		SimpleBeanPropertyFilter pensionFiltreList =  SimpleBeanPropertyFilter.serializeAllExcept("id");
		FilterProvider filterProvider = new SimpleFilterProvider().addFilter("pensionFiltre", pensionFiltreList);
		MappingJacksonValue proJacksonValue = new  MappingJacksonValue(pensionRecuperationList);
		proJacksonValue.setFilters(filterProvider);
		return proJacksonValue;
	}
	
	/**
	 * La methode permet la recuperation d'un objet persiste en bdd avec en parametre l'id 
	 * @param id represente l'identifiant de notre objet persiste en bdd
	 * @return notre objet si le parametre est correct
	 */
	@GetMapping("/Calypso/Pension/{id}")
	public MappingJacksonValue recherchePensionID(@PathVariable Long id)
	{
		Pension pensionRecuperation = pensionService.recherchePensionID(id);
		if(pensionRecuperation.getType().equals("emptycase"))
		{
			SimpleBeanPropertyFilter pensionFiltreFilter =  SimpleBeanPropertyFilter.serializeAllExcept("id","type");
			FilterProvider filterProvider = new SimpleFilterProvider().addFilter("pensionFiltre", pensionFiltreFilter);
			MappingJacksonValue proJacksonValue = new  MappingJacksonValue(pensionRecuperation);
			proJacksonValue.setFilters(filterProvider);
			return proJacksonValue;
		}
		SimpleBeanPropertyFilter pensionFiltre =  SimpleBeanPropertyFilter.serializeAllExcept("id");
		FilterProvider filterProvider = new SimpleFilterProvider().addFilter("pensionFiltre", pensionFiltre);
		MappingJacksonValue proJacksonValue = new  MappingJacksonValue(pensionRecuperation);
		proJacksonValue.setFilters(filterProvider);
		return proJacksonValue;
	}
	
	/**
	 * La methode nous permet de recuperer un objet par son nom en parametre
	 * @param nom represente l'attribut par lequel on veut retrouver l'objet en base
	 * @return une liste d'objet si le parametre rentre est bon
	 */
	@GetMapping("/Calypso/PensionName/{nom}")
	public MappingJacksonValue rechercheName(@PathVariable String nom)
	{
		List<Pension> pensionRetourList = pensionService.rechercheName(nom);
		SimpleBeanPropertyFilter pensionRetourFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id");
		FilterProvider filterProvider = new SimpleFilterProvider().addFilter("pensionFiltre", pensionRetourFiltre);
		MappingJacksonValue proJacksonValue = new MappingJacksonValue(pensionRetourList);
		proJacksonValue.setFilters(filterProvider);
		return proJacksonValue;
	}
	
	/**
	 * la methode permet de persister un objet en BDD
	 * @param pension represente l'objet que l'on veut persister en base de donnee
	 * @return un URI que l'on construit en fonction du resultat de l'operation
	 */
	@PostMapping("/Calypso/PensionSauvegarde/Sauvegarde")
	public ResponseEntity<Void> sauvegardePension(@RequestBody Pension pension)
	{
		boolean pensionVerification = pensionService.sauvegardePension(pension);
		if(pensionVerification!=false)
		{
			URI pensionConstructionUri = ServletUriComponentsBuilder
					.fromCurrentRequest()
					.path("/{id}")
					.buildAndExpand(pension.getId())
					.toUri();
			return ResponseEntity.created(pensionConstructionUri).build();
		}
		return ResponseEntity.noContent().build();
	}
	
	/**
	 * La methode va nous permettre de supprimer un objet en base de donnee 
	 * @param transporteurID represente le transport de l'attribut de l'objet que l'on veut supprimer
	 * @return un URI que l'on construit en fonction du resultat de l'operation 
	 */
	@PostMapping("/Calypso/PensionSupp/Suppression")
	public ResponseEntity<Void> suppressionPension(@RequestBody TransporteurID transporteurID)
	{
		boolean verificationPensionSuppression = pensionService.suppressionPension(transporteurID);
		if(verificationPensionSuppression!=false)
		{
			URI pensionConstructionUri = ServletUriComponentsBuilder
					.fromCurrentRequest()
					.path("/{id}")
					.buildAndExpand(transporteurID.getId())
					.toUri();
			return ResponseEntity.created(pensionConstructionUri).build();
		}
		return ResponseEntity.noContent().build();
	}
}
