package com.calypso.backnd.calypso10.beans;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFilter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name="role")
@JsonFilter("roleFiltre")
@SequenceGenerator(name = "id_role_seq",allocationSize = 1)
public class Role {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "id_role_seq")
	private Long id;
	@Column
	private String role;
	
	@OneToMany(mappedBy = "role",cascade = CascadeType.ALL)
	@JsonBackReference(value = "role_back")
	private List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
	


}
