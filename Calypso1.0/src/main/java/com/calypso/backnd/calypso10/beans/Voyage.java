package com.calypso.backnd.calypso10.beans;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFilter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Entity
@NoArgsConstructor
@Table(name = "voyage")
@SequenceGenerator(name = "id_voyage_seq",allocationSize = 1)
@JsonFilter("filtreVoyage")
public class Voyage {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "id_voyage_seq")
	@Column(name = "voyage_id")
	private Long id ;
	@Column
	private String nom;
	@Column
	private String dateDebut;
	@Column
	private String dateFin;
	@Column
	private int nombreVoyageur;
	@Column
	private Boolean itinerance;
	@Column
	private float prix;
	@Column
	private String detail;
	@Column
	private String pays;
	
	@OneToOne
	Pension pension;
	
	@OneToOne
	Theme theme;
	
	@OneToMany(mappedBy = "voyage")
	@JsonBackReference(value = "hebergement_back")
	private List<Hebergement> hebergement = new ArrayList<Hebergement>();
	
	@OneToMany(mappedBy = "voyage")
	@JsonBackReference(value = "activite_back")
	private List<Activite> activite = new ArrayList<Activite>();
	
	  
	@OneToMany(mappedBy = "voyage") 
	@JsonBackReference(value = "transport_back")
	private List<TransportInterne> transportInterne = new ArrayList<TransportInterne>();
	 
	
	@ManyToOne
	private Utilisateur utilisateur;



}
