package com.calypso.backnd.calypso10.activiteService;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.calypso.backnd.calypso10.activitedao.IActiviteDao;
import com.calypso.backnd.calypso10.beans.Activite;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;

@Service
public class ActiviteService implements IActiviteService {
	
	/*injection de notre couche dao */
	@Autowired
	IActiviteDao iActiviteDao;

	/**
	 * On recupere toutes les activites dans une liste
	 */
	@Override
	public List<Activite> recherche() {
		List <Activite> listActivite = iActiviteDao.findAll();
		return listActivite;
	}

	/**
	 * On recupere une activite selon son id
	 */
	@Override
	public Activite rechercheId(Long id) {
		Optional<Activite> verificationActiviteOptional = iActiviteDao.findById(id);
		if(verificationActiviteOptional.isPresent())
		{
			return verificationActiviteOptional.get();
		}
		Activite activite = new Activite();
		activite.setNom("emptycase");
		return activite;
	}

	/**
	 * On recupere des activites selon un nom
	 */
	@Override
	public List <Activite> rechercheNom(String nom) {
		List <Activite> listActiviteNom = iActiviteDao.rechercheNom(nom);
		return listActiviteNom;
	}

	/**
	 * On recupere une activite selon son adresse
	 */
	@Override
	public Activite rechercheAdresse(String adresse) {
		Activite ActiviteAdresse = iActiviteDao.rechercheAdresse(adresse);
		return ActiviteAdresse;
	}

	/**
	 * On recupere des activites selon le pays
	 */
	@Override
	public List<Activite> recherchePays(String pays) {
		List <Activite> listActivitePays = iActiviteDao.recherchePays(pays);
		return listActivitePays;
	}

	/**
	 * On recupere des activites selon la region
	 */
	@Override
	public List<Activite> rechercheRegion(String region) {
		List <Activite> listActiviteRegion = iActiviteDao.rechercheRegion(region);
		return listActiviteRegion;
	}

	/**
	 * On sauvegarde une nouvelle activite
	 */
	@Override
	public boolean sauvegardeActivite(Activite activite) {
		Activite confirmationSauvegarde = iActiviteDao.save(activite);
		if(confirmationSauvegarde!=null) {
			return true;
		}
		return false;
		
	}

	/**
	 * On supprime une activite
	 */
	@Override
	public boolean suppressionActivite(TransporteurID transporteurID) {
		Activite activite = iActiviteDao.findById(transporteurID.getId()).get();
		if(activite!=null) {
			iActiviteDao.deleteById(transporteurID.getId());			
			return true;
		}
		return false;
	}

}
