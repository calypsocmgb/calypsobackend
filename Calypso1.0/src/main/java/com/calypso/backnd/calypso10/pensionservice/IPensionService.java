package com.calypso.backnd.calypso10.pensionservice;

import java.util.List;

import com.calypso.backnd.calypso10.beans.Pension;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;

/**
 * L'interface nous permet de gerer les relations entre la couche controller et la couche dao
 * @author Groupe Calypso
 *
 */
public interface IPensionService {

	/**
	 * 	La methode permet de recuperer les pensions presente en BDD
	 * @return Une liste de Pension persiste en BDD
	 */
	public List<Pension> rechercheListePension();
	
	/**
	 * La methode nous permet de recuperer un objet en base de donnee avec son id en parametre 
	 * @param id represente l'identifiant de notre objet en bdd
	 * @return un objet pension si le parametre est correct
	 */
	public Pension recherchePensionID(Long id);
	
	/**
	 * La methode va nous permettre de recuperer un objet en BDD avec comme parametre un nom
	 * @param nom represente l'attribut par lequel on va recherche notre objet
	 * @return l'objet si le parametre rentre est correct
	 */
	public List<Pension> rechercheName(String nom);
	
	/**
	 * La methode va nous permettre de persister notre objet en BDD
	 * @param pension represente l'objet que l'on veut persister instancie
	 * @return un boolean afin de s'assurer que l'operation c'est bien deroule
	 */
	public Boolean sauvegardePension(Pension pension);
	
	/**
	 * La methode va nous permettre de supprimer un objet en BDD
	 * @param transporteurID represente l'ID de l'objet que l'on veut supprimer
	 * @return un boolean afin de savoir si l'operation c'est bien deroule
	 */
	public Boolean suppressionPension(TransporteurID transporteurID);
	
}
