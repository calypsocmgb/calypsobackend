package com.calypso.backnd.calypso10.transportInterneservice;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.calypso.backnd.calypso10.beans.TransportInterne;
import com.calypso.backnd.calypso10.transportInternedao.ITransportInterneDao;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;

@Service
public class TransportInterneService implements ITransportInterneService {
	
	/*injection de notre couche dao */
	@Autowired
	ITransportInterneDao iTransportInterneDao;

	/**
	 * On obtient tous les transports en interne existants
	 */
	@Override
	public List <TransportInterne> recherche() {
		List <TransportInterne> listTransportInterne = iTransportInterneDao.findAll();
		return listTransportInterne;
	}

	/**
	 * On obtient un transport en interne selon son id
	 */
	@Override
	public TransportInterne rechercheId(Long id) {
		Optional<TransportInterne> verificationTransportInterneOptional = iTransportInterneDao.findById(id);
		if(verificationTransportInterneOptional.isPresent())
		{
			return verificationTransportInterneOptional.get();
		}
		TransportInterne transportInterne = new TransportInterne();
		transportInterne.setNom("emptycase");
		return transportInterne;
	}

	/**
	 * On obtient une liste de transports internes selon la date de depart
	 */
	@Override
	public List <TransportInterne> rechercheLocationDepart(String locationDepart) {
		List <TransportInterne> listTransportInterneLocationDepart = iTransportInterneDao.rechercheTransportInterneLocationDepart(locationDepart);
		return listTransportInterneLocationDepart;
	}

	/**
	 * On obtient une liste de transports internes selon la date d'arrivee
	 */
	@Override
	public List<TransportInterne> rechercheLocationArrivee(String locationArrivee) {
		List <TransportInterne> listTransportInterneLocationArrivee = iTransportInterneDao.rechercheTransportInterneLocationArrivee(locationArrivee);
		return listTransportInterneLocationArrivee;
	}

	/**
	 * On sauvegarde un transport interne
	 */
	@Override
	public boolean sauvegardeTransportInterne(TransportInterne transportInterne) {
		TransportInterne confirmationSauvegarde = iTransportInterneDao.save(transportInterne);
		if(confirmationSauvegarde!=null) {
			return true;
		}
		return false;
	}

	/**
	 * On supprime un transport interne
	 */
	@Override
	public boolean suppressionTransportInterne(TransporteurID transporteurID) {
		TransportInterne transportInterne = iTransportInterneDao.findById(transporteurID.getId()).get();
		if(transportInterne!=null) {
			iTransportInterneDao.deleteById(transporteurID.getId());			
			return true;
		}
		return false;
	}

}
