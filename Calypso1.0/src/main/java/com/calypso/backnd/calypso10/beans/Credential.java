
package com.calypso.backnd.calypso10.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "credential")
@SequenceGenerator(name = "id_credential_seq",allocationSize = 1)
public class Credential {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "id_credential_seq")
	private Long id;
	@Column
	private String hashToken;
	
}
