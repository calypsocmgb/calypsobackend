package com.calypso.backnd.calypso10.hebergementservice;

import java.util.List;

import com.calypso.backnd.calypso10.beans.Hebergement;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;


/**
 * L'interface IHebergementService represente la couche métier
 * de notre modele mvc permettant la passerelle avec la couche
 * dao
 * @author Groupe Calypso
 *
 */
public interface IHebergementService {
	
	/**
	 * Permet d'obtenir tous les hebergements
	 * @return Tous les hebergements dans une liste
	 */
	public List<Hebergement> recherche();
	
	/**
	 * Permet d'obtenir un hebergement par son ID
	 * @param id Represente l'id de l'hebergement
	 * @return Un hebergement selon son ID
	 */
	public Hebergement rechercheId(Long id);
	
	/**
	 * Permet de recuperer des hebergements par pays
	 * @param pays Represente le pays des hebergements recherches
	 * @return Une liste d'hebergements par pays
	 */
	public List <Hebergement> recherchePays(String pays);
	
	/**
	 * Permet de recuperer des hebergements par region
	 * @param region Represente la region des hebergements recherches
	 * @return Une liste d'hebergements par region
	 */
	public List <Hebergement> rechercheRegion(String region);
	
	/**
	 * Permet d'obtenir un hebergement par son adresse
	 * @param adresse Represente l'adresse de l'hebergement
	 * @return Un hebergement selon son adresse
	 */
	public Hebergement rechercheAdresse(String adresse);
	
	/**
	 * Permet de recuperer des hebergements par nom en prevoyant les homonymes
	 * @param nom Represente le nom de l'hebergement
	 * @return Une liste d'hebergements par nom
	 */
	public List <Hebergement> rechercheNom(String nom);
	
	/**
	 * Permet de recuperer des hebergements par type
	 * @param type Represente le type de lhebergement par son nombre d'etoiles
	 * @return Une liste d'hebergements par type
	 */
	public List <Hebergement> rechercheType(String type);
	
	/**
	 * Permet la sauvegarde d'un nouveau Hebergement
	 * @param hebergement Represente l'objet à sauvegarder
	 * @return La confirmation ou non de la sauvegarde
	 */
	public boolean sauvegardeHebergement(Hebergement hebergement);
	
	/**
	 * Permet la suppression d'un nouveau Hebergement
	 * @param transporteurID Represente l'id de l'objet a supprimer
	 * @return La confirmation ou non de la supression
	 */
	public boolean suppressionHebergement (TransporteurID transporteurID);
	

}
