package com.calypso.backnd.calypso10.themeservice;

import java.util.List;

import com.calypso.backnd.calypso10.beans.Theme;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;

/**
 * L'interface IthemeService va nous permettre de gerer la couche intermediaire entre la dao et le controller
 * @author Groupe Calypso
 *
 */


public interface IThemeService {

	/**
	 * La methode va nous permettre de recuperer une liste de nos objets en base de donnees
	 * @return une liste de nos objets recuperes
	 */
	public List<Theme> themeList();
	
	/**
	 * La methode va nous permettre de recuperer un objet selon un parametre en base de donnee
	 * @param id represente le parametre par lequel on va effectuer la recherche
	 * @return l'objet si le parametre est correct
	 */
	public Theme rechercheThemeID(Long id);
	
	
	/**
	 * La methode nous permet de recherche notre objet en base de donnee avec un parametre
	 * @param nom represente le parametre par lequel on veut retrouver notre objet en bdd
	 * @return une liste des objets correspondant aux parametres rentre si celui-ci est correct
	 */
	public List<Theme> rechercheThemeName(String nom);
	
	
	/**
	 * La methode va nous permettre de faire persister en bdd nos objets
	 * @param theme represente l'objet que l'on veut persister 
	 * @return un booleen afin de verifier que l'operation c'est correctement deroule
	 */
	public Boolean sauvegardeTheme(Theme theme);
	
	
	/**
	 * La methode va nous permettre de supprimer un objet en BDD selon le parametre remplie
	 * @param transporteurID represente le parametre par lequel on veut retrouver notre objet a supprimer
	 * @return un booleen afin que l'on sache si la suppression c'est correctement deroule
	 */
	public Boolean suppressionTheme(TransporteurID transporteurID);
}
