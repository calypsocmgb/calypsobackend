package com.calypso.backnd.calypso10.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonFilter;
import lombok.Data;

@Data
@Entity
@Table(name="transportInterne")
@SequenceGenerator(name = "id_transport_seq",allocationSize = 1)
@JsonFilter("transportInterneFiltre")
public class TransportInterne {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "id_transport_seq")
	private int id;
	@Column
	private String nom;
	@Column
	private String locationDepart;
	@Column
	private String locationArrivee;
	@Column
	private float prix;
	
	@ManyToOne
	Voyage voyage;
	

}
