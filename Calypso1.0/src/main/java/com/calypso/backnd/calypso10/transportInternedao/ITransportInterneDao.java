package com.calypso.backnd.calypso10.transportInternedao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.calypso.backnd.calypso10.beans.TransportInterne;

/**
 * L'interface ITransportInterneDao represente notre couche interrogeant la BDD afin
 * de recuperer les informations necessaire a l'application
 * @author Groupe Calypso
 *
 */

@Repository
public interface ITransportInterneDao extends JpaRepository<TransportInterne, Long>{

	/**
	 * Requete qui permet d'obtenir les transports internes selon une date de depart
	 * @param locationDepart Represente la date de depart du transport interne
	 * @return Une liste de transports internes selon la date de depart
	 */
	@Query("select t from TransportInterne t where t.locationDepart = :locationDepart")
	List <TransportInterne> rechercheTransportInterneLocationDepart(@Param ("locationDepart") String locationDepart) ;
	
	/**
	 * Requete qui permet d'obtenir les transports internes selon une date d'arrivee
	 * @param locationArrivee Represente la date d'arrivee du transport interne
	 * @return Une liste de transports internes selon la date d'arrivee
	 */
	@Query("select t from TransportInterne t where t.locationArrivee = :locationArrivee")
	List <TransportInterne> rechercheTransportInterneLocationArrivee(@Param ("locationArrivee") String locationArrivee) ;
	
	
	
	
	
}
