package com.calypso.backnd.calypso10.controller;

import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.calypso.backnd.calypso10.beans.Utilisateur;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;
import com.calypso.backnd.calypso10.utilisateurservice.IUtilisateurService;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

/**
 * Cette classe représente la classe controller pour la classe Utilisateur elle
 * comprendra les url a attaquer pour recevoir les informations concernant les
 * utilisateurs
 * 
 * @author Groupe Calypso
 *
 */

@RestController
@CrossOrigin("http://localhost:4200")
public class UtilisateurController {

	/* injection de notre couche service */
	@Autowired
	IUtilisateurService utilisateurService;

	 /**
	 * recuperation de tous les utilisateurs
	 * @return une liste contenant tous les utilisateurs
	 */
	
	  @GetMapping("/Calypso/UtilisateurListe")
	  public MappingJacksonValue utilisateursList() {
	  
	  /*Utilisation d'un filtre sur les resultats afin de selectionner les champs que  l'on veut */
	  List<Utilisateur> utilisateurRecuperationListeList = utilisateurService.listeUtilisateurs(); 
	  /*champ a eviter */
	  SimpleBeanPropertyFilter utilisateurFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","credential",  "voyage","role");
	  FilterProvider listeFilterProvider = new  SimpleFilterProvider().addFilter("filtreUtilisateur", utilisateurFiltre);
	  /* construction du JSON de reponse */ 
	  MappingJacksonValue proJacksonValue = new  MappingJacksonValue(utilisateurRecuperationListeList);
	  proJacksonValue.setFilters(listeFilterProvider);
	  return proJacksonValue; 
	  }
	 

	/**
	 * La methode permet de recuperer un utilisateur en fonction de son id
	 * 
	 * @param id represente l'id de la personne
	 * @return les information concernant l'utilisateur demande
	 * @throws UtilisateurNotFound 
	 */
	@GetMapping("/Calypso/Utilisateur/{id}")
	public MappingJacksonValue utilisateurID(@PathVariable Long id) {

		  Utilisateur utilisateurID = utilisateurService.rechercheUtilisateur(id);
		  if (utilisateurID.getNom().equals("emptycase")) 
		  {
			  SimpleBeanPropertyFilter utilisateurFiltreEmpty = SimpleBeanPropertyFilter.serializeAllExcept("nom","id","age","credential","voyage","role","prenom","telephone","mail","adresse");
			  FilterProvider listeEmptyFilterProvider = new SimpleFilterProvider().addFilter("filtreUtilisateur", utilisateurFiltreEmpty);
			  MappingJacksonValue proJacksonValue = new MappingJacksonValue(utilisateurID);
			  proJacksonValue.setFilters(listeEmptyFilterProvider);
			  return proJacksonValue;
		  }
		  SimpleBeanPropertyFilter utilisateurFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id", "credential", "voyage","role"); 
		  FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("filtreUtilisateur", utilisateurFiltre);
		  MappingJacksonValue proJacksonValue = new MappingJacksonValue(utilisateurID);
		  proJacksonValue.setFilters(listeFilterProvider); 
		  return proJacksonValue;
		 
	}

	/**
	 * La methode permet de recuperer les utilisateurs en fonction de leur nom
	 * 
	 * @param nom represente le nom de famille de l'utilisateur recherche
	 * @return une liste d'utilisateur [attention homonyme]
	 */
	@GetMapping("/Calypso/UtilisateurName/{nom}")
	public MappingJacksonValue utilisateurNom(@PathVariable String nom) {
		
		  /*Utilisation d'un filtre sur les resultats afin de selectionner les champs que  l'on veut */
		  List<Utilisateur> utilisateurRecuperationListe = utilisateurService.rechercheListNom(nom);
		  /*champ a eviter */
		  SimpleBeanPropertyFilter utilisateurFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","credential",  "voyage","role");
		  FilterProvider listeFilterProvider = new  SimpleFilterProvider().addFilter("filtreUtilisateur", utilisateurFiltre);
		  /* construction du JSON de reponse */ 
		  MappingJacksonValue proJacksonValue = new  MappingJacksonValue(utilisateurRecuperationListe);
		  proJacksonValue.setFilters(listeFilterProvider);
		  return proJacksonValue; 
	}

	/**
	 * la methode nous permet de persiste un utilisateur en BDD 
	 * @param utilisateur represente l'utilisateur du site a persiste en BDD 
	 * @return un code personnalise afin de verifier l'integrite de la transaction
	 */
	@PostMapping("/Calypso/Utilisateur/Sauvegarde")
	public ResponseEntity<Void> sauvegardeUtilisateur(@RequestBody Utilisateur utilisateur) {
		boolean confirmationPersistence = utilisateurService.sauvegardeUtilisateur(utilisateur);
		if(confirmationPersistence!=false)
		{
			/*creation de notre code de renvois personnalise afin de controler la persistance de nos objets*/
			URI utilisateurUriSave = ServletUriComponentsBuilder
					.fromCurrentRequest()
					.path("/{id}")
					.buildAndExpand(utilisateur.getId())
					.toUri();
			return ResponseEntity.created(utilisateurUriSave).build();
		}
		/*renvois du code sans contenue signifiant le mauvais deroulement de la procedure*/
		return ResponseEntity.noContent().build();
	}

	/**
	 * La methode permet d'attaquer l'url afin de supprimer un utilisateur en BDD
	 * 
	 * @param id represente l'identifiant de notre objet persiste
	 */
	@PostMapping("/Calypso/Utilisateur/suppression")
	public ResponseEntity<Void> suppressionUtilisateur(@RequestBody TransporteurID transporteurID) {
		boolean confirmationSuppresion = utilisateurService.suppressionUtilisateur(transporteurID);
		if(confirmationSuppresion!=false)
		{
			URI utilisateurSuppression = ServletUriComponentsBuilder
					.fromCurrentRequest()
					.path("/{id}")
					.buildAndExpand(transporteurID.getId())
					.toUri();
			return ResponseEntity.created(utilisateurSuppression).build();
			}
		return ResponseEntity.noContent().build();
	}
}
