package com.calypso.backnd.calypso10.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.calypso.backnd.calypso10.beans.TransportInterne;
import com.calypso.backnd.calypso10.transportInterneservice.ITransportInterneService;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

@RestController
@CrossOrigin("http://localhost:4200")
public class TransportInterneController {
	
	/* injection de notre couche service */
	@Autowired
	ITransportInterneService iTransportInterneService;
	
	/**
	 * Permet d'obtenir tous les transports en interne
	 * @return Une liste qui contient tous les transport interne
	 */
	@GetMapping("/Calypso/RechercheTransportInterne")
	public MappingJacksonValue rechercheTransportInterne()
	{
		List <TransportInterne> transportInterneList = iTransportInterneService.recherche();
		SimpleBeanPropertyFilter transportInterneFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","voyage");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("transportInterneFiltre", transportInterneFiltre);
		MappingJacksonValue transportInterneJacksonValue = new MappingJacksonValue(transportInterneList);
		transportInterneJacksonValue.setFilters(listeFilterProvider);
		return transportInterneJacksonValue ;
	}
	
	/**
	 * Permet d'obtenir un transport interne par son id
	 * @param id Represente l'id d'un transport
	 * @return Un transport interne selon son id
	 */
	@GetMapping("/Calypso/RechercheTransportInterne/{id}")
	public MappingJacksonValue TransportInterneId(@PathVariable Long id)
	{
		TransportInterne transportInterneVerification = iTransportInterneService.rechercheId(id);
		if(transportInterneVerification.getNom().equals("emptycase"))
		{
			SimpleBeanPropertyFilter transportInterneFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","voyage","nom","locationDepart","locationArrivee","prix");
			FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("transportInterneFiltre", transportInterneFiltre);
			MappingJacksonValue transportInterneJacksonValue = new MappingJacksonValue(transportInterneVerification);
			transportInterneJacksonValue.setFilters(listeFilterProvider);
			return transportInterneJacksonValue ;			
		}
		SimpleBeanPropertyFilter transportInterneFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("transportInterneFiltre", transportInterneFiltre);
		MappingJacksonValue transportInterneJacksonValue = new MappingJacksonValue(transportInterneVerification);
		transportInterneJacksonValue.setFilters(listeFilterProvider);
		return transportInterneJacksonValue ;
		
	}
	
	/**
	 * Permet d'obtenir les transports internes selon la date de depart
	 * @param locationDepart Represente la date de depart du transport interne
	 * @return Une liste de transport interne correpondant a la date de depart
	 */
	@GetMapping("/Calypso/RechercheTransportInterneLocationDepart/{locationDepart}")
	public MappingJacksonValue TransportInterneLocationDepart(@PathVariable String locationDepart)
	{
		List <TransportInterne> transportInterneList = iTransportInterneService.rechercheLocationDepart(locationDepart);
		SimpleBeanPropertyFilter transportInterneFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","voyage");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("transportInterneFiltre", transportInterneFiltre);
		MappingJacksonValue transportInterneJacksonValue = new MappingJacksonValue(transportInterneList);
		transportInterneJacksonValue.setFilters(listeFilterProvider);
		return transportInterneJacksonValue ;
	}
	
	/**
	 * Permet d'obtenir les transports internes selon la date d'arrivee
	 * @param locationArrivee Represente la date de d'arrivee du transport interne
	 * @return Une liste de transport interne correpondant a la date d'arrivee
	 */
	@GetMapping("/Calypso/RechercheTransportInterneLocationArrivee/{locationArrivee}")
	public MappingJacksonValue TransportInterneLocationArrivee(@PathVariable String locationArrivee)
	{
		List <TransportInterne> transportInterneList = iTransportInterneService.rechercheLocationArrivee(locationArrivee);
		SimpleBeanPropertyFilter transportInterneFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","voyage");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("transportInterneFiltre", transportInterneFiltre);
		MappingJacksonValue transportInterneJacksonValue = new MappingJacksonValue(transportInterneList);
		transportInterneJacksonValue.setFilters(listeFilterProvider);
		return transportInterneJacksonValue ;
	}
	
	/**
	 * Permet la sauvegarde d'un nouveau Transport Interne
	 * @param transportInterne Represente l'objet à sauvegarder
	 */
	@PostMapping("/Calypso/NouveauTransportInterne")
    public ResponseEntity<Void> sauvegardeTransportInterne(@RequestBody TransportInterne transportInterne )
    { 
		boolean transportInterneVerification = iTransportInterneService.sauvegardeTransportInterne(transportInterne);
		if (transportInterneVerification!=false) 
		{
			URI transportInterneUri =  ServletUriComponentsBuilder
					.fromCurrentRequest()
					.path("/{id}")
					.buildAndExpand(transportInterne.getId())
					.toUri();
			
			return ResponseEntity.created(transportInterneUri).build();
		}
		 return ResponseEntity.noContent().build();
    }
	
	/**
	 * Permet la suppression d'un nouveau Transport Interne
	 * @param transporteurID Represente l'id du transport interne a supprimer
	 */
	@PostMapping("/Calypso/SuppressionTransportInterne")
	public ResponseEntity<Void> supprimerTransportInterne(@RequestBody TransporteurID transporteurID)
	{
		boolean confirmationSuppression = iTransportInterneService.suppressionTransportInterne(transporteurID);
		if(confirmationSuppression!=false)
		{
			URI transporInterneSuppression = ServletUriComponentsBuilder
					.fromCurrentRequest()
					.path("/{id}")
					.buildAndExpand(transporteurID.getId())
					.toUri();
			return ResponseEntity.created(transporInterneSuppression).build(); 
		}
		return ResponseEntity.noContent().build();
	}

}
