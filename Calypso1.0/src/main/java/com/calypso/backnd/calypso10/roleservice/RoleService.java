package com.calypso.backnd.calypso10.roleservice;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.calypso.backnd.calypso10.beans.Role;
import com.calypso.backnd.calypso10.roledao.IRoleDao;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;

/**
 * La classe RoleService nous permet de faire l'intermediaire entre la 
 * couche dao et le controller
 * @author Groupe Calypso
 *
 */

@Service
public class RoleService implements IRoleService{

	
	/*injection de notre couche dao*/
	@Autowired
	IRoleDao roleDao;

	/**
	 * LA methode nous permet de recuperer une liste de notre objet persiste en bdd
	 */
	@Override
	public List<Role> rechercheRoleListe() {
		return roleDao.findAll();
	}

	/**
	 * La methode nous permet de recherche un objet persister en base de donnee grace a un parametre 
	 */
	@Override
	public Role rechercheRoleID(Long id) {
		
		Optional<Role> roleOptional = roleDao.findById(id);
		if(roleOptional.isPresent())
		{
			return roleOptional.get();
		}
		Role role = new Role();
		role.setRole("emptycase");
		return role;
	}

	/**
	 * La methode nous permet de recherche nos objet avec un parametre associe
	 */
	@Override
	public List<Role> rechercheRoleName(String nom) {
		return roleDao.rechercheRoleName(nom);
	}

	/**
	 * La methode nous permet de persister et de controler l'operation de persistence en base de donnee
	 */
	@Override
	public Boolean roleSauvegarde(Role role) {
		Role roleVerification = roleDao.save(role);
		if (roleVerification!=null) {
			return true;
		}
		return false;
	}

	/**
	 * La methode permet la suppression d'un objet persiste en base de donnee
	 */
	@Override
	public Boolean suppressionRole(TransporteurID transporteurID) {
		Optional<Role> verificationRoleOptional = roleDao.findById(transporteurID.getId());
		if(verificationRoleOptional.isPresent())
		{
			roleDao.deleteById(transporteurID.getId());
			return true;
		}
		return false;
	}
	
	
}
