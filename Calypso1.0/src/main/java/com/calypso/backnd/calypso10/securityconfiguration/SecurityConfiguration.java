package com.calypso.backnd.calypso10.securityconfiguration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration  extends WebSecurityConfigurerAdapter{

	
	/*
	 * @Override protected void configure(HttpSecurity http) throws Exception {
	 * http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.
	 * STATELESS).and(). authorizeRequests().antMatchers(HttpMethod.GET,
	 * "/Calypso/**").hasAnyRole("", "") .antMatchers(HttpMethod.POST,
	 * "/Calypso/**").hasAnyRole("", "") .antMatchers(HttpMethod.POST,
	 * "/Calypso/**").hasRole("") .antMatchers(HttpMethod.DELETE,
	 * "/Calypso/**").hasRole("").and(). requestCache().requestCache(new
	 * NullRequestCache()).and().
	 * httpBasic().authenticationEntryPoint(entree).and(). cors().and().
	 * csrf().disable(); }
	 */
	
	
	/**
	 * La methode va nous permettre d'eviter la generation de token a chaque actualisation du site 
	 * mais de provide un user unique avec les credential par defaut
	 */
	@Bean
    @Override
    public UserDetailsService userDetailsService() {
        @SuppressWarnings("deprecation")
		UserDetails user =
             User.withDefaultPasswordEncoder()
                .username("app-name")
                .password("calypso")
                .roles("AppMajoritaire")
                .build();

        return new InMemoryUserDetailsManager(user);
    }
	
	@Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.csrf().disable()
                .antMatcher("/**")
                .authorizeRequests().and()
                .httpBasic()
                .and()
                .authorizeRequests().anyRequest().authenticated().and().cors();
    }

}
