package com.calypso.backnd.calypso10.themeservice;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.calypso.backnd.calypso10.beans.Theme;
import com.calypso.backnd.calypso10.themedao.IThemeDao;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;

/**
 * La classe themeService va gerer les interactions entre la couche dao et controller
 * @author Groupe Calypso
 *
 */
@Service
public class ThemeService implements IThemeService{

	/*injection de notre couche dao*/
	@Autowired
	private IThemeDao themeDao;

	/**
	 * La methode va nous permettre de recuperer une liste de nos objets persiste
	 */
	@Override
	public List<Theme> themeList() {
		List<Theme> themeRecuperation = themeDao.findAll();
		return themeRecuperation;
	}

	/**
	 * La methode va nous permettre d'aller verifier puis de retrouver notre objet persister si le
	 * parametre rentre est correct
	 */
	@Override
	public Theme rechercheThemeID(Long id) {
		Optional<Theme> recuperationThemeOptional = themeDao.findById(id);
		if(recuperationThemeOptional.isPresent())
		{
			return recuperationThemeOptional.get();
		}
		Theme theme = new Theme();
		theme.setTheme("emptycase");
		return theme;
	}

	/**
	 * La methode va nous permettre de recuperer des objets persistes grace a un parametre rentre par l'utilisateur 
	 */
	@Override
	public List<Theme> rechercheThemeName(String nom) {
		return themeDao.rechercheListeTheme(nom);
 	}

	/**
	 * La methode va nous permettre de persister un objet en bdd
	 */
	@Override
	public Boolean sauvegardeTheme(Theme theme) {
		Theme themeVerification = themeDao.save(theme);
		if (themeVerification!=null) {
			return true;
		}
		return false;
	}

	/**
	 * La methode nous permet de supprimer mais aussi de verifier que l'objet supprimer existe en BDD
	 */
	@Override
	public Boolean suppressionTheme(TransporteurID transporteurID) {
		Optional<Theme> themeSuppressionFinder = themeDao.findById(transporteurID.getId());
		if(themeSuppressionFinder.isPresent())
		{
			themeDao.deleteById(transporteurID.getId());
			return true;
		}
		return false;
	}
	
	
	
	
}
