package com.calypso.backnd.calypso10.utilisateurservice;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.calypso.backnd.calypso10.beans.Utilisateur;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;
import com.calypso.backnd.calypso10.utilisateurdao.IUtilisateurDao;

/**
 * La classe UtilisateurService implement l'interface IUtilisateurService
 * elle va injecter la couche dao afin d'executer les requetes
 * @author Groupe Calypso
 *
 */

@Service
public class UtilisateurService implements IUtilisateurService{

	/*injection de notre couche dao */
	@Autowired
	IUtilisateurDao utilisateurDao;

	/**
	 * On recupere la liste global des utilisateurs [fonctionADM]
	 */
	@Override
	public List<Utilisateur> listeUtilisateurs() {
		return utilisateurDao.findAll();
	}

	
	/**
	 * La methode permet de recuperer un seul utilisateur
	 * @throws UtilisateurNotFoundException 
	 */
	@Override
	public Utilisateur rechercheUtilisateur(Long id)   {
		Optional<Utilisateur> utilisateurVerification = utilisateurDao.findById(id);
		if(utilisateurVerification.isPresent())
		{
			return utilisateurVerification.get();
		}
		Utilisateur utilisateurVide = new Utilisateur();
		utilisateurVide.setNom("emptycase");
		return utilisateurVide;
	}
	/**
	 * La methode nous permet de recherche des utilisateurs par le nom
	 */
	@Override
	public List<Utilisateur> rechercheListNom(String nom) {
		return utilisateurDao.rechercherListNom(nom);
	}
	
	/**
	 * la methode permet la sauvegarde de l'utilisateur en BDD
	 */
	@Override
	public Boolean sauvegardeUtilisateur(Utilisateur utilisateur)
	{
		Utilisateur confirmationUtilisateur = utilisateurDao.save(utilisateur);
		if(confirmationUtilisateur!=null) {
			return true;
		}
		return false;
	}

	/**
	 * La methode permet de supprimer un utilisateur dans la BDD [attention accessibilité fonction ADM]
	 * 
	 */
	@Override
	public Boolean suppressionUtilisateur(TransporteurID transporteurID) {
		Optional<Utilisateur> utilisateurVerificationl = utilisateurDao.findById(transporteurID.getId());
		if(utilisateurVerificationl.isPresent()) {
			utilisateurDao.deleteById(transporteurID.getId());			
			return true;
		}
		return false;
	}
	
	
	
}
