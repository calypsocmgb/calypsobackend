package com.calypso.backnd.calypso10.activitedao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.calypso.backnd.calypso10.beans.Activite;

/**
 * L'interface IActiviteDao represente notre couche interrogeant la BDD afin
 * de recuperer les informations necessaire a l'application
 * @author Groupe Calypso
 *
 */

@Repository
public interface IActiviteDao extends JpaRepository<Activite, Long> {
	
	/**
	 * Requete qui permet d'obtenir des activites par nom
	 * @param nom Represente le nom de l'activite
	 * @return Une liste d'activites selon le nom
	 */
	@Query("select a from Activite a where a.nom = :nom")
	List <Activite> rechercheNom(@Param ("nom") String nom) ;
	
	/**
	 * Requete qui permet d'obtenir une activite par son adresse
	 * @param adresse Represente l'adresse de l'activite
	 * @return Une activite correspondant a l'adresse
	 */
	@Query("select a from Activite a where a.adresse = :adresse")
	Activite rechercheAdresse(@Param ("adresse") String adresse) ;
	
	/**
	 * Requete qui permet d'obtenir des activites par pays
	 * @param pays Reprente le pays ou se situe l'activite
	 * @return Une liste d'activites du pays desire
	 */
	@Query("select a from Activite a where a.pays = :pays")
	List <Activite> recherchePays(@Param ("pays") String pays) ;
	
	/**
	 * Requete qui permet d'obtenir des activites par region
	 * @param region Reprente la region ou se situe l'activite
	 * @return Une liste d'activites de la region desiree
	 */
	@Query("select a from Activite a where a.region = :region")
	List <Activite> rechercheRegion(@Param ("region") String region) ;
	
	

}
