package com.calypso.backnd.calypso10.voyageservice;

import java.util.List;

import com.calypso.backnd.calypso10.beans.Voyage;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;

/**
 * L'interface IVoyageService nous permet de faire
 * l'intermediaire entre la couche DAO et le controller
 * @author Groupe Calypso 
 *
 */


public interface IVoyageService {
	
	/**
	 * La methode renvois les voyages disponible sous forme de liste
	 * @return une liste avec les voyages disponubles
	 */
	public List<Voyage> listeVoyage();
	
	/**
	 * La methode nous permet de recherche un voyage par son id 
	 * @param id represente l'identifiant de l'objet que l'on recherche
	 * @return un objet voyage  
	 */
	public Voyage recherVoyageID(Long id);
	
	/**
	 * La methode permet de recuperer une liste de voyage correspondant au nom recherche
	 * celle-ci est vide si pas de correspondance
	 * @param nom represente l'appelation du voayeg que l'on recherche
	 * @return une liste vide si non trouvee
	 */
	public List<Voyage> rechercheVoyage(String nom);
	
	/**
	 * La methode nous permet de persiste un objet en BDD de type voyage 
	 * @param voyage correspond  a l'entite que l'on veut persiste
	 * @return la dit entite si bon deroulement de l'operation
	 */
	public Boolean voyageSauvegarde(Voyage voyage);

	/**
	 * La methode permet la suppression d'un objet en BDD de type voyage
	 * @param transporteurID representant l'ID de l'objet a supprimer
	 * @return un boolean de maniere a confirmer ou non la suppression
	 */
	public Boolean voyageSuppression(TransporteurID transporteurID);

	
	/**
	 * La methode permet de recuperer une liste de voyage correspondant au pays recherche
	 * celle-ci est vide si pas de correspondance
	 * @param pays represente le pays du voayage que l'on recherche
	 * @return une liste vide si non trouvee
	 */
	public List<Voyage> recherchePays(String pays);
	
}
