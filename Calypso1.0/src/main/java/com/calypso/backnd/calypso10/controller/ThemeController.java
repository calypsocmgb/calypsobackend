package com.calypso.backnd.calypso10.controller;

import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.calypso.backnd.calypso10.beans.Theme;
import com.calypso.backnd.calypso10.themeservice.IThemeService;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

/**
 * La classe nous permet de gerer les requetes que l'on reçoit
 * @author Groupe Calypso
 *
 */

@RestController
@CrossOrigin("*")
public class ThemeController {

	/*injection de notre couche Service*/
	@Autowired
	private IThemeService themeService;
	
	
	/**
	 * La methode permet de recuperer tous nos themes persiste en bdd
	 * @return la liste de nos objet persiste en bdd
	 */
	@GetMapping("/Calypso/ThemeListe")
	public MappingJacksonValue rechercheListeTheme()
	{
		List<Theme> listeRecuperationThemes = themeService.themeList();
		SimpleBeanPropertyFilter themeFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("themeFiltre",themeFiltre );
		MappingJacksonValue themeJacksonValue = new MappingJacksonValue(listeRecuperationThemes);
		themeJacksonValue.setFilters(listeFilterProvider);
		return themeJacksonValue;
	}
	
	/**
	 * La methode nous permet d'aller chercher en bdd des objets grace a un  parametre 
	 * @param id represente le parametre par lequel la recherche va s'effectuer en bdd
	 * @return notre objet si le parametre rentre est correct
	 */
	@GetMapping("/Calypso/Theme/{id}")
	public MappingJacksonValue rechercheThemeID(@PathVariable Long id)
	{
		Theme theme = themeService.rechercheThemeID(id);
		if(theme.getTheme().equals("emptycase"))
		{
			SimpleBeanPropertyFilter themeFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","theme");
			FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("themeFiltre",themeFiltre );
			MappingJacksonValue themeJacksonValue = new MappingJacksonValue(theme);
			themeJacksonValue.setFilters(listeFilterProvider);
			return themeJacksonValue;
		}
		SimpleBeanPropertyFilter themeFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("themeFiltre",themeFiltre );
		MappingJacksonValue themeJacksonValue = new MappingJacksonValue(theme);
		themeJacksonValue.setFilters(listeFilterProvider);
		return themeJacksonValue;
	}
	
	/**
	 * La methode nous permet de recuperer des objets persister avec un parametre
	 * @param nom represente le parametre par lequel on veut retrouver notre objet en bdd
	 * @return notre objet si le parametre rentre est correct
	 */
	@GetMapping("/Calypso/ThemeName/{nom}")
	public MappingJacksonValue rechercheThemeName(@PathVariable String nom)
	{
		List<Theme> recuperationThemeNameList= themeService.rechercheThemeName(nom);
		SimpleBeanPropertyFilter themeFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("themeFiltre",themeFiltre );
		MappingJacksonValue themeJacksonValue = new MappingJacksonValue(recuperationThemeNameList);
		themeJacksonValue.setFilters(listeFilterProvider);
		return themeJacksonValue;
	}
	
	/**
	 * La methode va nous permettre de persister nos objet en BDD
	 * @param theme represente l'objet que l'on veut introduire dans notre base de donnee
	 * @return un URI construit nous informant du bon deroulement ou non de l'operation
	 */
	@PostMapping("/Calypso/ThemeSauvegarde/Sauvegarde")
	public ResponseEntity<Void> sauvegardeTheme(@RequestBody Theme theme)
	{
		boolean retourVerificationSauvegarde = themeService.sauvegardeTheme(theme);
		if (retourVerificationSauvegarde!=false) {
			URI themeConstructionUri = ServletUriComponentsBuilder
					.fromCurrentRequest()
					.path("/{id}")
					.buildAndExpand(theme.getId())
					.toUri();
			return ResponseEntity.created(themeConstructionUri).build();
		}
		return ResponseEntity.noContent().build();
	}
	
	@PostMapping("/Calypso/ThemeSupp/Supression")
	public ResponseEntity<Void> suppressionTheme(@RequestBody TransporteurID transporteurID)
	{
		boolean retourVerificationSuppression = themeService.suppressionTheme(transporteurID);
		if(retourVerificationSuppression!=false)
		{
			URI themeConstructionUri = ServletUriComponentsBuilder
					.fromCurrentRequest()
					.path("/{id}")
					.buildAndExpand(transporteurID.getId())
					.toUri();
			return ResponseEntity.created(themeConstructionUri).build();
		}
		return ResponseEntity.noContent().build();
	}
	
	
	
	
}
