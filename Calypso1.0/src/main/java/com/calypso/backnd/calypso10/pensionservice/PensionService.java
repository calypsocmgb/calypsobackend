package com.calypso.backnd.calypso10.pensionservice;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.calypso.backnd.calypso10.beans.Pension;
import com.calypso.backnd.calypso10.pensiondao.IPensionDao;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;

/**
 * La classe PensionService represente l'intermediaire entre notre couche dao et nos controllers
 * @author Groupe Calypso
 *
 */
@Service
public class PensionService implements IPensionService{
	
	/*injection de notre dao */
	@Autowired
	IPensionDao pensionDao;
	
	/**
	 * 	La methode permet de recuperer les pensions presente en BDD
	 * @return Une liste de Pension persiste en BDD
	 */
	@Override
	public List<Pension> rechercheListePension()
	{
		return pensionDao.findAll();
	}

	/**
	 * La methode nous permet de recuperer un objet en base de donnee avec son id en parametre 
	 * @param id represente l'identifiant de notre objet en bdd
	 * @return un objet pension si le parametre est correct
	 */
	@Override
	public Pension recherchePensionID(Long id) {
		Optional<Pension> verificationPensionRecuperation = pensionDao.findById(id);
		if (verificationPensionRecuperation.isPresent()!=false) {
			return verificationPensionRecuperation.get();
		}
		Pension pension = new Pension();
		pension.setType("emptycase");
		return pension;
	}
	
	/**
	 * La methode nous permet de recuperer un objet en base avec son nom comme parametre
	 * @param nom represente l'attribut par lequel on veut recherche notre objet
	 * @return une liste avec les objets si le parametre est correct
	 */
	@Override
	public List<Pension> rechercheName(String nom) {
		return pensionDao.rechercheName(nom);
	}
	
	/**
	 * La methode va nous permettre de persister un objet en base de donnees
	 * @param pension represente l'objet que l'on veut persister en BDD
	 * @return un booleen afin de verifier le bon deroulement de l'operation
	 */
	@Override
	public Boolean sauvegardePension(Pension pension) {
		Pension pensionVerification = pensionDao.save(pension);
		if(pensionVerification!=null)
		{
			return true;
		}
		return false;
	}

	/**
	 * La methode va nous permettre de supprimer un objet en base de donnee
	 * @param transporteurID represente l'id de l'objet que l'on veut supprimer
	 * @return un booleen afin de verifier le bon deroulement de l'operation
	 */
	@Override
	public Boolean suppressionPension(TransporteurID transporteurID) {
		Optional<Pension> pensionVerificationOptional = pensionDao.findById(transporteurID.getId());
		if(pensionVerificationOptional.isPresent())
		{
			pensionDao.deleteById(transporteurID.getId());
			return true;
		}
		return false;
	}
	
	
	
}
