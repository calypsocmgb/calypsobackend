package com.calypso.backnd.calypso10.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.calypso.backnd.calypso10.beans.Hebergement;
import com.calypso.backnd.calypso10.beans.Voyage;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;
import com.calypso.backnd.calypso10.voyageservice.IVoyageService;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

/**
 * La classe VoyageController nous permet de rechercher
 * les differentes informations concernant les voyages 
 * de notre applications
 * @author Groupe Calypso
 *
 */

@RestController
@CrossOrigin("*")
public class VoyageController {

	/*injection de notre class voyage service*/
	@Autowired
	IVoyageService voyageService;
	
	/**
	 * La methode nous permet d'afficher la liste des voyages disponibles
	 * @return une liste comprenant les voyages disponibles 
	 */
	@GetMapping("/Calypso/Voyages")
	public MappingJacksonValue rechercheVoyages()
	{
		List<Voyage> voyageConfirmation =  voyageService.listeVoyage();
		/*configuration du filtre*/
		SimpleBeanPropertyFilter voyageFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","utilisateur");
		FilterProvider listeFilterProvider = new  SimpleFilterProvider().addFilter("filtreVoyage", voyageFiltre).addFilter("pensionFiltre", SimpleBeanPropertyFilter.serializeAllExcept("id")).addFilter("themeFiltre", SimpleBeanPropertyFilter.serializeAllExcept("id"));
		/* construction du JSON de reponse */ 
		MappingJacksonValue proJacksonValue = new  MappingJacksonValue(voyageConfirmation);
		proJacksonValue.setFilters(listeFilterProvider);
		return proJacksonValue; 
	}

	/**
	 * La methode va nous permettre de cherche un voyage par son id 
	 * @param id represente l'identifiant du voyage dans notre BDD
	 * @return notre objet mappe si celui-ci existe bien 
	 */
	@GetMapping("/Calypso/Voyage/{id}")
	public MappingJacksonValue rechercheVoyagesID(@PathVariable Long id )
	{
		Voyage voyageConfirmation = voyageService.recherVoyageID(id);
		if(voyageConfirmation.getNom().equals("emptycase"))
		{
			/*on supprime les champs que le JSON soit vide */
			SimpleBeanPropertyFilter voyageEmptyFilter = SimpleBeanPropertyFilter.serializeAllExcept("id","nom","dateDebut","dateFin","nombreVoyageur","itinerance","prix","pension","theme","hebergement","activite","transportIntern","utilisateur");
			FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("filtreVoyage", voyageEmptyFilter).addFilter("pensionFiltre", SimpleBeanPropertyFilter.serializeAllExcept("id")).addFilter("themeFiltre", SimpleBeanPropertyFilter.serializeAllExcept("id"));
			MappingJacksonValue proJacksonValue = new MappingJacksonValue(voyageConfirmation);
			proJacksonValue.setFilters(listeFilterProvider);
			return proJacksonValue;
		}
		/*cas ou le voyage est bel et bien dans la BDD*/
		SimpleBeanPropertyFilter voyageFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","utilisateur");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("filtreVoyage", voyageFiltre).addFilter("pensionFiltre", SimpleBeanPropertyFilter.serializeAllExcept("id")).addFilter("themeFiltre", SimpleBeanPropertyFilter.serializeAllExcept("id"));
		MappingJacksonValue proJacksonValue = new MappingJacksonValue(voyageConfirmation);
		proJacksonValue.setFilters(listeFilterProvider);
		return proJacksonValue;
	}
	
	/**
	 * La methode permet de rechercher des voyages par le nom
	 * @param nom represente l'appelation du voyage que l'on veut recherche 
	 * @return une liste de voyage contenant les voyages sinon vide
	 */
	@GetMapping("/Calypso/VoyageName/{nom}")
	public MappingJacksonValue rechercheVoyageName(@PathVariable String nom)
	{
		List<Voyage> listeVoyages = voyageService.rechercheVoyage(nom);
		/*definition du filtre*/
		SimpleBeanPropertyFilter voyageNameFilter = SimpleBeanPropertyFilter.serializeAllExcept("id","utilisateur");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("filtreVoyage", voyageNameFilter).addFilter("pensionFiltre", SimpleBeanPropertyFilter.serializeAllExcept("id")).addFilter("themeFiltre", SimpleBeanPropertyFilter.serializeAllExcept("id"));
		MappingJacksonValue proJacksonValue = new MappingJacksonValue(listeVoyages);
		proJacksonValue.setFilters(listeFilterProvider);
		return proJacksonValue;
	}
	
	/**
	 * La methode permet d'ajouter un voyage dans la BDD
	 * @param voyage reprend le JSON recu avec les informations necessaires a la persistence
	 * @return un code personnalise permettant la verification de la creation
	 */
	@PostMapping("/Calypso/Voyage/Sauvegarde")
	public ResponseEntity<Void> sauvegardeVoyage(@RequestBody Voyage voyage)
	{
		boolean voyageVerification = voyageService.voyageSauvegarde(voyage);
		if(voyageVerification!=false)
		{
		URI voyageSauvegardeUri = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(voyage.getId())
				.toUri();
		return ResponseEntity.created(voyageSauvegardeUri).build();
		}
		return ResponseEntity.noContent().build();
	}
	
	/**
	 * La methode permet la suppression d'un objet persistant dans la BDD
	 * @param transporteurID represente l'identifiant de l'objet a supprimer
	 * @return un url permettant la verification du deroulement de l'operation
	 */
	@PostMapping("/Calypso/Voyage/Suppression")
	public ResponseEntity<Void> suppressionVoyage(@RequestBody TransporteurID transporteurID)
	{
		boolean voyageSuppressionVerificaiton = voyageService.voyageSuppression(transporteurID);
		if(voyageSuppressionVerificaiton!=false)
		{
		URI voyageSuppressionUri = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(transporteurID.getId())
				.toUri();
		return ResponseEntity.created(voyageSuppressionUri).build();
		}
		return ResponseEntity.noContent().build();
	}
	
	
	/**
	 * Recuperation des voyages par pays
	 * @param pays represente le pays ou le voyage se situe
	 * @return Une liste de voyages dans le pays
	 */
	@GetMapping("/Calypso/RechercheVoyagesPays/{pays}")
	public MappingJacksonValue voyagePays(@PathVariable String pays)
	{	
		List <Voyage> listVoyages = voyageService.recherchePays(pays);
		SimpleBeanPropertyFilter voyagePaysFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","utilisateur");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("filtreVoyage", voyagePaysFiltre).addFilter("pensionFiltre", SimpleBeanPropertyFilter.serializeAllExcept("id")).addFilter("themeFiltre", SimpleBeanPropertyFilter.serializeAllExcept("id"));
		MappingJacksonValue proJacksonValue = new MappingJacksonValue(listVoyages);
		proJacksonValue.setFilters(listeFilterProvider);
		return  proJacksonValue;
	}
	
	
}
