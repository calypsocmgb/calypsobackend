package com.calypso.backnd.calypso10.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "activite")
@Entity
@SequenceGenerator(name = "id_activite_seq",allocationSize = 1)
@JsonFilter("activiteFiltre")
public class Activite {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "id_activite_seq")
	private Long id;
	@Column
	private String nom;
	@Column
	private String adresse;
	@Column
	private float prix;
	@Column
	private String pays;
	@Column
	private String region;

	
	@ManyToOne
	private Voyage voyage;
	

	
	
	
}
