package com.calypso.backnd.calypso10.hebergementservice;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.calypso.backnd.calypso10.beans.Hebergement;
import com.calypso.backnd.calypso10.hebergementdao.IHebergementDao;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;

@Service
public class HebergementService implements IHebergementService {
	
	/*injection de notre couche dao */
	@Autowired
	IHebergementDao iHebergementDao;

	/**
	 * On recupere tous les hebergements dans une liste
	 */
	@Override
	public List<Hebergement> recherche() {
		List <Hebergement> listHebergement = iHebergementDao.findAll();
		return listHebergement;
	}

	/**
	 * Permet de recuperer un hebergement par son ID
	 */
	@Override
	public Hebergement rechercheId(Long  id) {
		Optional<Hebergement> verificationHebergement = iHebergementDao.findById(id);
		if(verificationHebergement.isPresent())
		{
			return verificationHebergement.get();
		}
		Hebergement hebergement = new Hebergement();
		hebergement.setNom("emptycase");
		return hebergement;
	}

	/**
	 * Permet de recuperer des hebergements par pays
	 */
	@Override
	public List<Hebergement> recherchePays(String pays) {
		List <Hebergement> listHebergementPays = iHebergementDao.recherchePays(pays);	
		return listHebergementPays;
	}

	/**
	 * Permet de recuperer des hebergements par region
	 */
	@Override
	public List<Hebergement> rechercheRegion(String region) {
		List <Hebergement> listHerbergementRegion = iHebergementDao.rechercheRegion(region);
		return listHerbergementRegion;
	}

	/**
	 * Permet de recuperer un hebergement par son adresse
	 */
	@Override
	public Hebergement rechercheAdresse(String adresse) {
		Hebergement HebergementAdresse = iHebergementDao.rechercheAdresse(adresse);
		return HebergementAdresse;
	}

	/**
	 * Permet de recuperer des hebergements par nom en prévoyant les homonymes
	 */
	@Override
	public List<Hebergement> rechercheNom(String nom) {
		List <Hebergement> listHebergementNom = iHebergementDao.rechercheNom(nom);
		return listHebergementNom;
	}

	/**
	 * Permet de recuperer des hebergements par type
	 */
	@Override
	public List<Hebergement> rechercheType(String type) {
		List <Hebergement> listHebergementType = iHebergementDao.rechercheType(type);
		return listHebergementType;
	}
	
	/**
	 * On sauvegarde un nouveau hebergement
	 */
	@Override
	public boolean sauvegardeHebergement(Hebergement hebergement) {
		Hebergement confirmationSauvegarde = iHebergementDao.save(hebergement);
		if(confirmationSauvegarde!=null) {
			return true;
		}
		return false;
	}

	
	/**
	 * On supprime un hebergement
	 */
	@Override
	public boolean suppressionHebergement(TransporteurID transporteurID) {
		Hebergement hebergement = iHebergementDao.findById(transporteurID.getId()).get();
		if(hebergement!=null) {
			iHebergementDao.deleteById(transporteurID.getId());			
			return true;
		}
		return false;
	}

}
