package com.calypso.backnd.calypso10.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFilter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="theme")
@JsonFilter("themeFiltre")
public class Theme {

	@Id
	private Long id;
	@Column
	private String theme;
	
	

	
}
