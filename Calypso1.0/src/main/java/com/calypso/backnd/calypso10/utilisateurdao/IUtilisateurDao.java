package com.calypso.backnd.calypso10.utilisateurdao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.calypso.backnd.calypso10.beans.Utilisateur;

/**
 * L'interface IUtilisateurDao represente notre couche interrogeant la BDD afin
 * de recuperer les informations necessaire a l'application
 * 
 * @author Groupe Calypso
 *
 */

@Repository
public interface IUtilisateurDao extends JpaRepository<Utilisateur, Long> {

	/**
	 * La methode permet de recuperer les utilisateurs ayant le nom recherches
	 * 
	 * @param nomUtil le nom recherche
	 * @return une liste des utilisateurs ayant ce nom
	 */
	 @Query( value =
		  "SELECT id, adresse, age, mail, nom, prenom, telephone, credential_id, role_id FROM utilisateur where utilisateur.nom=:nomUtil"
		  , nativeQuery = true) public List<Utilisateur> rechercherListNom(@Param
		  ("nomUtil") String nomUtil);	 
		 
}
