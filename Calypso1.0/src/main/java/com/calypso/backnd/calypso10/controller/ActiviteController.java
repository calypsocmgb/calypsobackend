package com.calypso.backnd.calypso10.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.calypso.backnd.calypso10.activiteService.IActiviteService;
import com.calypso.backnd.calypso10.beans.Activite;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;


@RestController
@CrossOrigin("http://localhost:4200")
public class ActiviteController {
	
	/* injection de notre couche service */
	@Autowired
	IActiviteService iActiviteService; 
	

	/**
	 * Recuperation de toutes les activites
	 * @return Une liste qui contient toutes les activites
	 */
	@GetMapping("/Calypso/RechercheActivite")
	public MappingJacksonValue rechercheActivite()
	{
		List <Activite> activiteList = iActiviteService.recherche();
		SimpleBeanPropertyFilter activiteFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","voyage");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("activiteFiltre", activiteFiltre);
		MappingJacksonValue activiteJacksonValue = new MappingJacksonValue(activiteList);
		activiteJacksonValue.setFilters(listeFilterProvider);
		return activiteJacksonValue;
	}
	
	/**
	 * Recuperation d'une activite par son id
	 * @param id Represente l'id de l'activite
	 * @return Une activite selon son id
	 */
	@GetMapping("/Calypso/RechercheActivite/{id}")
	public MappingJacksonValue activiteId(@PathVariable Long id)
	{
		Activite activiteRecuperation = iActiviteService.rechercheId(id);
		if(activiteRecuperation.getNom().equals("emptycase"))
		{
			SimpleBeanPropertyFilter activiteFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","voyage","nom","adresse","region","pays","prix");
			FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("activiteFiltre", activiteFiltre);
			MappingJacksonValue activiteJacksonValue = new MappingJacksonValue(activiteRecuperation);
			activiteJacksonValue.setFilters(listeFilterProvider);
			return activiteJacksonValue ;	
		}
		SimpleBeanPropertyFilter activiteFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","voyage");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("activiteFiltre", activiteFiltre);
		MappingJacksonValue activiteJacksonValue = new MappingJacksonValue(activiteRecuperation);
		activiteJacksonValue.setFilters(listeFilterProvider);
		return activiteJacksonValue ;
		
	}
	
	/**
	 * Recuperation d'activites selon leur nom
	 * @param nom Represente le nom de l'activite
	 * @return Un liste d'activites ayant le meme nom
	 */
	@GetMapping("/Calypso/RechercheActiviteNom/{nom}")
	public MappingJacksonValue activiteNom(@PathVariable String nom)
	{
	
		List<Activite> activiteList = iActiviteService.rechercheNom(nom);
		SimpleBeanPropertyFilter activiteFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","voyage");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("activiteFiltre", activiteFiltre);
		MappingJacksonValue activiteJacksonValue = new MappingJacksonValue(activiteList);
		activiteJacksonValue.setFilters(listeFilterProvider);
		return activiteJacksonValue ;
	}
	
	/**
	 * Recuperation d'une activite par son adresse
	 * @param adresse Represente le lieu de l'activite
	 * @return Une activite par son nom
	 */
	@GetMapping("/Calypso/RechercheActiviteAdresse/{adresse}")
	public MappingJacksonValue activiteAdresse(@PathVariable String adresse)
	{
		
		Activite activiteRecuperation = iActiviteService.rechercheAdresse(adresse);
		SimpleBeanPropertyFilter activiteFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","voyage");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("activiteFiltre", activiteFiltre);
		MappingJacksonValue activiteJacksonValue = new MappingJacksonValue(activiteRecuperation);
		activiteJacksonValue.setFilters(listeFilterProvider);
		return activiteJacksonValue ;
	}
	
	/**
	 * Recuperation d'activites selon le pays
	 * @param pays Represente le pays ou se situe l'activite
	 * @return Une liste d'activites par pays
	 */
	@GetMapping("/Calypso/RechercheActivitePays/{pays}")
	public MappingJacksonValue activitePays(@PathVariable String pays)
	{

		List<Activite> activiteList = iActiviteService.recherchePays(pays);
		SimpleBeanPropertyFilter activiteFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","voyage");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("activiteFiltre", activiteFiltre);
		MappingJacksonValue activiteJacksonValue = new MappingJacksonValue(activiteList);
		activiteJacksonValue.setFilters(listeFilterProvider);
		return activiteJacksonValue ;
	}
	
	/**
	 * Recuperation d'activites selon la region
	 * @param region Represente la region ou se situe l'activite
	 * @return Une liste d'activites par region
	 */
	@GetMapping("/Calypso/RechercheActiviteRegion/{region}")
	public MappingJacksonValue activiteRegion(@PathVariable String region)
	{

		List<Activite> activiteList = iActiviteService.rechercheRegion(region);
		SimpleBeanPropertyFilter activiteFiltre = SimpleBeanPropertyFilter.serializeAllExcept("id","voyage");
		FilterProvider listeFilterProvider = new SimpleFilterProvider().addFilter("activiteFiltre", activiteFiltre);
		MappingJacksonValue activiteJacksonValue = new MappingJacksonValue(activiteList);
		activiteJacksonValue.setFilters(listeFilterProvider);
		return activiteJacksonValue ;
	}

	/**
	 * Sauvegarde d'une nouvelle activite
	 * @param activite Represente l'objet a sauvegarder
	 */
	@PostMapping("/Calypso/NouveauActivite")
    public ResponseEntity<Void> sauvegardeActivite(@RequestBody Activite activite )
    {
       boolean activiteVerification =  iActiviteService.sauvegardeActivite(activite);
       if(activiteVerification!=false)
       {
    	   URI activiteUri = ServletUriComponentsBuilder
    			   .fromCurrentRequest()
    			   .path("{/id}")
    			   .buildAndExpand(activite.getId())
    			   .toUri();
    	   return ResponseEntity.created(activiteUri).build();
       }
        return ResponseEntity.noContent().build();
    }
	
	/**
	 * Supprime une activite
	 * @param transporteurID Represente l'id de l'acitivte a supprimer
	 * @return 
	 */
	@PostMapping("/Calypso/SuppressionActivite")
	public ResponseEntity<Void> supprimerActivite(@RequestBody TransporteurID transporteurID)
	{
		boolean confirmationSuppression = iActiviteService.suppressionActivite(transporteurID);
		if(confirmationSuppression!=false)
		{
			URI activiteSuppression = ServletUriComponentsBuilder
					.fromCurrentRequest()
					.path("/{id}")
					.buildAndExpand(transporteurID.getId())
					.toUri();
			return ResponseEntity.created(activiteSuppression).build();
		}
		return ResponseEntity.noContent().build();
	}
	
	
	
}
