package com.calypso.backnd.calypso10.utilisateurservice;

import java.util.List;

import com.calypso.backnd.calypso10.beans.Utilisateur;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;

/**
 * L'interface IUtilisateurService représente la couche métier
 * de notre modele mvc permettant la passerelle avec la couche 
 * dao
 * @author Groupe Calypso
 *
 */


public interface IUtilisateurService {

	
	/**
	 * methode permettant la recuperation des utilisateurs de l'applications [fonction]
	 * @return la liste des utilisateurs presentes en BDD
	 */
	public List<Utilisateur> listeUtilisateurs();
	
	/**
	 * methode recuperation d'un utilisateur par l'ID
	 * @param id represente l'ID de notre utilisateur recherche
	 * @return l'utilisateur si celui-ci est bien present en base
	 * @throws UtilisateurNotFound 
	 */
	public Utilisateur rechercheUtilisateur(Long id);
	
	/**
	 * methode pour rechercher les utilisateurs par noms
	 * @param nom de la personne recherche en BDD
	 * @return la liste des utilisateurs trouve pour ce parametre (attention homonyme)
	 */
	public List<Utilisateur> rechercheListNom(String nom);
	
	/**
	 * La methode permet de sauvegarde un utilisateur dans la base de donnée
	 * @param utilisateur represente l'entite utilisateur
	 * @return un boolean afin de savoir si l'operation a ete effectue correctement
	 */
	public Boolean sauvegardeUtilisateur (Utilisateur utilisateur);
	
	/**
	 * La meethode permet la suppression d'un utilisateur dans la BDD 
	 * @param id represente l'identifiant de notre objet persiste 
	 * @return un boolean afin de confirmer la suppression 
	 */
	public Boolean suppressionUtilisateur(TransporteurID transporteurID);
	
}
