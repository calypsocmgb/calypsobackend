package com.calypso.backnd.calypso10.transportInterneservice;

import java.util.List;

import com.calypso.backnd.calypso10.beans.TransportInterne;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;



/**
 * L'interface ITransportInterneService represente la couche métier
 * de notre modele mvc permettant la passerelle avec la couche
 * dao
 * @author Groupe Calypso
 *
 */

public interface ITransportInterneService {

	/**
	 * Permet d'obtenir tous les transports internes existants
	 * @return Une liste de tous les transports
	 */
	public List<TransportInterne> recherche();
	
	/**
	 * Permet d'obtenir un transport interne selon un id
	 * @param id Représente l'id du transport interne
	 * @return Un transport interne selon son id
	 */
	public TransportInterne rechercheId(Long id);
	
	/**
	 * Permet d'obtenir des transports en interne selon la date de depart
	 * @param locationDepart Represente la date de depart du transport interne
	 * @return Une liste de transports internes selon la date de depart
	 */
	public List <TransportInterne> rechercheLocationDepart(String locationDepart);
	
	/**
	 * Permet d'obtenir des transports en interne selon la date d'arrivee
	 * @param locationArrivee Represente la date d'arrivee du transport interne 
	 * @return Une liste de transports internes selon la date d'arrivee
	 */
	public List <TransportInterne> rechercheLocationArrivee(String locationArrivee);
	
	/**
	 * Permet la sauvegarde d'un transport interne
	 * @param transportInterne Represente l'objet a sauvegarder
	 * @return Confirmation ou non de la sauvegarde
	 */
	public boolean sauvegardeTransportInterne(TransportInterne transportInterne);
	
	/**
	 * Permet la suppression d'un transport interne
	 * @param transporteurID Represente l'id du transport interne a supprimer
	 * @return Confirmation ou non de la suppression
	 */
	public boolean suppressionTransportInterne (TransporteurID transporteurID);
	
	
}
