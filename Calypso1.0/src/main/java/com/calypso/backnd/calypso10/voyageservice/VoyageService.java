package com.calypso.backnd.calypso10.voyageservice;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.calypso.backnd.calypso10.beans.Hebergement;
import com.calypso.backnd.calypso10.beans.Voyage;
import com.calypso.backnd.calypso10.transporteur.TransporteurID;
import com.calypso.backnd.calypso10.voyagedao.IVoyageDao;

/**
 * La classe VoaygeService nous permet de faire 
 * l'intermediaire entre la couche DAO et la couche Controller
 * @author Groupe Calypso
 *
 */

@Service
public class VoyageService implements IVoyageService {
	
	/*injection de notre couche dao*/
	@Autowired
	IVoyageDao voyageDao;

	/**
	 * la methode nous permet de retourner tous les voyages 
	 */
	@Override
	public List<Voyage> listeVoyage() {
		return voyageDao.findAll();
	}

	/**
	 * 
	 */
	@Override
	public Voyage recherVoyageID(Long id) {
		
		Optional<Voyage> voyageVerification = voyageDao.findById(id);
		if(voyageVerification.isPresent()!=false)
		{
			return voyageVerification.get();
		}
		Voyage voyage = new Voyage();
		voyage.setNom("emptycase");
		return voyage;
		
	}

	/**
	 * La methode nous permet de rechercher les voyages en fonction du nom renseigne
	 */
	@Override
	public List<Voyage> rechercheVoyage(String nom) {
		return voyageDao.rechercheVoyageNom(nom);
		
	}
	
	/**
	 * La methode permet de persister un objet de type voyage en BDD
	 */
	@Override
	public Boolean voyageSauvegarde(Voyage voyage) {
		Voyage voyageVerification  = voyageDao.save(voyage);
		if(voyageVerification!=null)
		{
			return true;
		}
		return false;
	}

	/**
	 * La methode permet la suppression d'un objet en base de donnee
	 */
	@Override
	public Boolean voyageSuppression(TransporteurID transporteurID) {
		
		Optional<Voyage>voyageVerificationExistence = voyageDao.findById(transporteurID.getId());
		if(voyageVerificationExistence.isPresent())
		{
			voyageDao.deleteById(transporteurID.getId());
			return true;
		}
		return false;
	}
	
	/**
	 * Permet de recuperer des voyages par pays
	 */
	@Override
	public List<Voyage> recherchePays(String pays) {
		List <Voyage> listVoyagesPays = voyageDao.rechercheVoyagePays(pays);
		return listVoyagesPays;
	}
	

}
